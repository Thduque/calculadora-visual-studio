﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.tres = New System.Windows.Forms.Button()
        Me.dos = New System.Windows.Forms.Button()
        Me.uno = New System.Windows.Forms.Button()
        Me.cuatro = New System.Windows.Forms.Button()
        Me.cinco = New System.Windows.Forms.Button()
        Me.seis = New System.Windows.Forms.Button()
        Me.cero = New System.Windows.Forms.Button()
        Me.siete = New System.Windows.Forms.Button()
        Me.ocho = New System.Windows.Forms.Button()
        Me.nueve = New System.Windows.Forms.Button()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.RESULTADO = New System.Windows.Forms.Button()
        Me.BORRAR = New System.Windows.Forms.Button()
        Me.Multiplicacion = New System.Windows.Forms.Button()
        Me.Division = New System.Windows.Forms.Button()
        Me.Resta = New System.Windows.Forms.Button()
        Me.suma = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'tres
        '
        Me.tres.Location = New System.Drawing.Point(200, 49)
        Me.tres.Name = "tres"
        Me.tres.Size = New System.Drawing.Size(58, 32)
        Me.tres.TabIndex = 0
        Me.tres.Text = "3"
        Me.tres.UseVisualStyleBackColor = True
        '
        'dos
        '
        Me.dos.Location = New System.Drawing.Point(141, 49)
        Me.dos.Name = "dos"
        Me.dos.Size = New System.Drawing.Size(58, 32)
        Me.dos.TabIndex = 1
        Me.dos.Text = "2"
        Me.dos.UseVisualStyleBackColor = True
        '
        'uno
        '
        Me.uno.Location = New System.Drawing.Point(82, 49)
        Me.uno.Name = "uno"
        Me.uno.Size = New System.Drawing.Size(58, 32)
        Me.uno.TabIndex = 2
        Me.uno.Text = "1"
        Me.uno.UseVisualStyleBackColor = True
        '
        'cuatro
        '
        Me.cuatro.Location = New System.Drawing.Point(82, 82)
        Me.cuatro.Name = "cuatro"
        Me.cuatro.Size = New System.Drawing.Size(58, 32)
        Me.cuatro.TabIndex = 5
        Me.cuatro.Text = "4"
        Me.cuatro.UseVisualStyleBackColor = True
        '
        'cinco
        '
        Me.cinco.Location = New System.Drawing.Point(141, 82)
        Me.cinco.Name = "cinco"
        Me.cinco.Size = New System.Drawing.Size(58, 32)
        Me.cinco.TabIndex = 4
        Me.cinco.Text = "5"
        Me.cinco.UseVisualStyleBackColor = True
        '
        'seis
        '
        Me.seis.Location = New System.Drawing.Point(200, 82)
        Me.seis.Name = "seis"
        Me.seis.Size = New System.Drawing.Size(58, 32)
        Me.seis.TabIndex = 3
        Me.seis.Text = "6"
        Me.seis.UseVisualStyleBackColor = True
        '
        'cero
        '
        Me.cero.Location = New System.Drawing.Point(141, 148)
        Me.cero.Name = "cero"
        Me.cero.Size = New System.Drawing.Size(58, 32)
        Me.cero.TabIndex = 10
        Me.cero.Text = "0"
        Me.cero.UseVisualStyleBackColor = True
        '
        'siete
        '
        Me.siete.Location = New System.Drawing.Point(82, 115)
        Me.siete.Name = "siete"
        Me.siete.Size = New System.Drawing.Size(58, 32)
        Me.siete.TabIndex = 8
        Me.siete.Text = "7"
        Me.siete.UseVisualStyleBackColor = True
        '
        'ocho
        '
        Me.ocho.Location = New System.Drawing.Point(141, 115)
        Me.ocho.Name = "ocho"
        Me.ocho.Size = New System.Drawing.Size(58, 32)
        Me.ocho.TabIndex = 7
        Me.ocho.Text = "8"
        Me.ocho.UseVisualStyleBackColor = True
        '
        'nueve
        '
        Me.nueve.Location = New System.Drawing.Point(200, 115)
        Me.nueve.Name = "nueve"
        Me.nueve.Size = New System.Drawing.Size(58, 32)
        Me.nueve.TabIndex = 6
        Me.nueve.Text = "9"
        Me.nueve.UseVisualStyleBackColor = True
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(82, 23)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(176, 20)
        Me.TextBox1.TabIndex = 11
        '
        'RESULTADO
        '
        Me.RESULTADO.Location = New System.Drawing.Point(41, 115)
        Me.RESULTADO.Name = "RESULTADO"
        Me.RESULTADO.Size = New System.Drawing.Size(41, 32)
        Me.RESULTADO.TabIndex = 14
        Me.RESULTADO.Text = "="
        Me.RESULTADO.UseVisualStyleBackColor = True
        '
        'BORRAR
        '
        Me.BORRAR.Location = New System.Drawing.Point(41, 82)
        Me.BORRAR.Name = "BORRAR"
        Me.BORRAR.Size = New System.Drawing.Size(41, 32)
        Me.BORRAR.TabIndex = 13
        Me.BORRAR.Text = "CE"
        Me.BORRAR.UseVisualStyleBackColor = True
        '
        'Multiplicacion
        '
        Me.Multiplicacion.Location = New System.Drawing.Point(41, 49)
        Me.Multiplicacion.Name = "Multiplicacion"
        Me.Multiplicacion.Size = New System.Drawing.Size(41, 32)
        Me.Multiplicacion.TabIndex = 12
        Me.Multiplicacion.Text = "*"
        Me.Multiplicacion.UseVisualStyleBackColor = True
        '
        'Division
        '
        Me.Division.Location = New System.Drawing.Point(2, 115)
        Me.Division.Name = "Division"
        Me.Division.Size = New System.Drawing.Size(41, 32)
        Me.Division.TabIndex = 17
        Me.Division.Text = "\"
        Me.Division.UseVisualStyleBackColor = True
        '
        'Resta
        '
        Me.Resta.Location = New System.Drawing.Point(2, 82)
        Me.Resta.Name = "Resta"
        Me.Resta.Size = New System.Drawing.Size(41, 32)
        Me.Resta.TabIndex = 16
        Me.Resta.Text = "-"
        Me.Resta.UseVisualStyleBackColor = True
        '
        'suma
        '
        Me.suma.Location = New System.Drawing.Point(2, 49)
        Me.suma.Name = "suma"
        Me.suma.Size = New System.Drawing.Size(41, 32)
        Me.suma.TabIndex = 15
        Me.suma.Text = "+"
        Me.suma.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 215)
        Me.Controls.Add(Me.Division)
        Me.Controls.Add(Me.Resta)
        Me.Controls.Add(Me.suma)
        Me.Controls.Add(Me.RESULTADO)
        Me.Controls.Add(Me.BORRAR)
        Me.Controls.Add(Me.Multiplicacion)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.cero)
        Me.Controls.Add(Me.siete)
        Me.Controls.Add(Me.ocho)
        Me.Controls.Add(Me.nueve)
        Me.Controls.Add(Me.cuatro)
        Me.Controls.Add(Me.cinco)
        Me.Controls.Add(Me.seis)
        Me.Controls.Add(Me.uno)
        Me.Controls.Add(Me.dos)
        Me.Controls.Add(Me.tres)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents tres As System.Windows.Forms.Button
    Friend WithEvents dos As System.Windows.Forms.Button
    Friend WithEvents uno As System.Windows.Forms.Button
    Friend WithEvents cuatro As System.Windows.Forms.Button
    Friend WithEvents cinco As System.Windows.Forms.Button
    Friend WithEvents seis As System.Windows.Forms.Button
    Friend WithEvents cero As System.Windows.Forms.Button
    Friend WithEvents siete As System.Windows.Forms.Button
    Friend WithEvents ocho As System.Windows.Forms.Button
    Friend WithEvents nueve As System.Windows.Forms.Button
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents RESULTADO As System.Windows.Forms.Button
    Friend WithEvents BORRAR As System.Windows.Forms.Button
    Friend WithEvents Multiplicacion As System.Windows.Forms.Button
    Friend WithEvents Division As System.Windows.Forms.Button
    Friend WithEvents Resta As System.Windows.Forms.Button
    Friend WithEvents suma As System.Windows.Forms.Button

End Class
